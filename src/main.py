from objects.note_book import NoteBook
from objects.work_with_file import WorkWithFile


def main():
    notebook = NoteBook()
    work_with_file = WorkWithFile()
    work_with_file.check_file()
    while True:
        try:
            choice = int(input("МЕНЮ:\n"
                               "0 - вихід\n"
                               "1 - вивести на екран записну книжку\n"
                               "2 - додати новий запис\n"
                               "3 - видалити запис\n"
                               "4 - змінити запис\n"
                               "5 - пошук за іменем\n"
                               "6 - пошук за номером телефону\n"
                               "7 - сортування за іменем\n"
                               "8 - сортування за прізвищем\n"
                               "Зробіть свій вибір: "))
        except ValueError:
            print("Введіть, будь ласка, цифрове значення")
            continue
        match choice:
            case 1:
                notebook.print_notebook()
            case 2:
                notebook.new_note()
            case 3:
                notebook.del_note()
            case 4:
                notebook.change_note()
            case 5:
                notebook.find_note(0)
            case 6:
                notebook.find_note(1)
            case 7:
                notebook.sort_notes(0)
            case 8:
                notebook.sort_notes(1)
            case 0:
                print("*"*60)
                print("ДО НОВИХ ЗУСТРІЧЕЙ!")
                print("*"*60)
                break


if __name__ == "__main__":
    main()

