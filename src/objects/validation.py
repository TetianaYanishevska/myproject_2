import phonenumbers
from datetime import datetime


def check_string(string_to_test: str) -> bool:
    if not string_to_test.isalpha():
        print("Значення даного поля повинно складатися з букв")
    return string_to_test.isalpha()


def check_phone_number(phone_number_to_test) -> bool:
    try:
        phone_number = phonenumbers.parse(phone_number_to_test)
    except Exception:
        print("Невірний номер телефону")
        return False
    if not (phonenumbers.is_valid_number(phone_number) and phonenumbers.is_possible_number(phone_number)):
        print("Невірний номер телефону")
    return phonenumbers.is_valid_number(phone_number) and phonenumbers.is_possible_number(phone_number)


def check_date(date_to_test) -> bool:
    try:
        if datetime.strptime(date_to_test, '%d-%m-%Y') >= datetime.now():
            print("Введена дата більша за поточну")
            return False
        else:
            return True
    except ValueError:
        print("Невірний формат дати")
        return False


def check_number(number_to_test) -> bool:
    if not number_to_test.isdigit():
        print("Введіть числове значення")
    return number_to_test.isdigit()


def valid_data(idx: int, request: str):
    valid = False
    while not valid:
        data_to_check = input(request)
        match idx:
            case 0:
                if check_string(data_to_check):
                    valid = True
            case 1:
                if check_phone_number(data_to_check):
                    valid = True
            case 2:
                if check_date(data_to_check):
                    valid = True
            case 3:
                if check_number(data_to_check):
                    valid = True
    return data_to_check

