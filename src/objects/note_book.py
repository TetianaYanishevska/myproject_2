import operator
from tabulate import tabulate
import time
from objects.work_with_file import WorkWithFile
from objects.validation import valid_data


class NoteBook:
    def __init__(self):
        self.headers = ["Ім'я", "Прізвище", "Номер телефону", "Адреса", "Дата народження"]
        self.work_with_file = WorkWithFile()

    def check_notebook(self) -> bool:
        if len(self.work_with_file.read_file()) == 0:
            print("Записна книжка не містить даних")
            return False
        else:
            return True

    def print_notebook(self):
        if not self.check_notebook():
            return
        notebook = []
        for item in self.work_with_file.read_file():
            notebook.append(item.values())
        print(tabulate(notebook, headers=self.headers, tablefmt='pipe', stralign='center'))

    def new_note(self):
        print("-" * 60)
        print("Створення нового запису (поля з * обов'язкові для заповнення)")
        print("-" * 60)
        name = valid_data(0, "Ім'я*: ")
        last_name = valid_data(0, "Прізвище*: ")
        phone_number = valid_data(1, "Номер телефону в форматі +ХХХХХХХХХХХХ*: ")
        address = input("Адреса: ")
        date_of_birth = valid_data(2, "Дата народження в форматі ДД-ММ-РРРР: ")
        new_note = {"name": name,"last_name": last_name, "phone_number": phone_number, "address": address,
                    "date_of_birth": date_of_birth}
        self.work_with_file.write_file(new_note)
        print("<>" * 30)
        print("Запис успішно збережено")
        print("<>" * 30)

    def del_note(self):
        if not self.check_notebook():
            return
        note_to_del = valid_data(0, "Введіть прізвище, чий запис потрібно видалити: ")
        notebook = self.work_with_file.read_file()
        out_filter = list((filter(lambda x: x.get("last_name") != note_to_del, notebook)))
        if len(notebook) == len(out_filter):
            print("Даного прізвища немає в записній книжці")
            return
        else:
            self.work_with_file.update_file_with_new_data(out_filter)
            print("<>"*30)
            print("Запис успішно видалено")
            print("<>" * 30)

    def change_note(self):
        if not self.check_notebook():
            return
        li = self.work_with_file.read_file()
        notebook = []
        n = 1
        for item in li:
            item = {"number": n} | item
            n += 1
            notebook.append(item.values())
        print(tabulate(notebook, headers=["Номер", "Ім'я", "Прізвище", "Номер телефону",
                                          "Адреса", "Дата народження"], tablefmt='pipe', stralign='center'))
        number_to_change = int(valid_data(3, "Введіть номер запису, який потрібно змінити: "))
        if not [item for item in notebook if number_to_change in item]:
            print("Помилка: даного номера немає в списку")
            return
        while True:
            try:
                choice = int(input("Оберіть поле, яке потрібно змінити: \n"
                                "1 - ім'я\n"
                                "2 - прізвище\n"
                                "3 - номер телефону\n"
                                "4 - адреса\n"
                                "5 - дата народження\n"
                                "0 - вихід\n"))
            except ValueError:
                print("Введіть, будь ласка, цифрове значення")
                continue
            match choice:
                case 0:
                    print("Внесення змін завершено!")
                    print("-" * 60)
                    time.sleep(2)
                    break
                case 1:
                    name = valid_data(0, "Ім'я*: ")
                    li[number_to_change-1]["name"] = name
                    self.work_with_file.update_file_with_new_data(li)
                    print("<>" * 30)
                    print("Ім'я успішно змінено")
                    print("<>" * 30)
                case 2:
                    last_name = valid_data(0, "Прізвище*: ")
                    li[number_to_change - 1]["last_name"] = last_name
                    self.work_with_file.update_file_with_new_data(li)
                    print("<>" * 30)
                    print("Прізвище успішно змінено")
                    print("<>" * 30)
                case 3:
                    phone_number = valid_data(1, "Номер телефону в форматі +ХХХХХХХХХХХХ*: ")
                    li[number_to_change - 1]["phone_number"] = phone_number
                    self.work_with_file.update_file_with_new_data(li)
                    print("<>" * 30)
                    print("Номер телефону успішно змінено")
                    print("<>" * 30)
                case 4:
                    address = input("Адреса: ")
                    li[number_to_change - 1]["address"] = address
                    self.work_with_file.update_file_with_new_data(li)
                    print("<>" * 30)
                    print("Адресу успішно змінено")
                    print("<>" * 30)
                case 5:
                    date_of_birth = valid_data(2, "Дата народження в форматі ДД-ММ-РРРР: ")
                    li[number_to_change - 1]["date_of_birth"] = date_of_birth
                    self.work_with_file.update_file_with_new_data(li)
                    print("<>" * 30)
                    print("Дату народження успішно змінено")
                    print("<>" * 30)
                case _:
                    print("Невірний вибір, спробуйте ще раз")

    def find_note(self, idx: int):
        if not self.check_notebook():
            return
        match idx:
            case 0:
                note_to_find = valid_data(0, "Введіть ім'я для пошуку: ")
                out_filter = list((filter(lambda x: x.get("name").lower() == note_to_find.lower(),
                                          self.work_with_file.read_file())))
                if out_filter:
                    found_notes = []
                    for item in out_filter:
                        found_notes.append(item.values())
                    print(tabulate(found_notes, headers=self.headers, tablefmt='pipe', stralign='center'))
                else:
                    print("Записна книжка не містить записів з вказаним іменем")
            case 1:
                note_to_find = valid_data(1, "Введіть номер телефону для пошуку в форматі +ХХХХХХХХХХХХ: ")
                out_filter = list((filter(lambda x: x.get("phone_number") == note_to_find,
                                          self.work_with_file.read_file())))
                if out_filter:
                    found_notes = []
                    for item in out_filter:
                        found_notes.append(item.values())
                    print(tabulate(found_notes, headers=self.headers, tablefmt='pipe', stralign='center'))
                else:
                    print("Записна книжка не містить записів з вказаним номером телефону")

    def sort_notes(self, idx: int):
        if not self.check_notebook():
            return
        notebook = self.work_with_file.read_file()
        match idx:
            case 0:
                notebook.sort(key=operator.itemgetter('name'))
            case 1:
                notebook.sort(key=operator.itemgetter('last_name'))
        sorted_notes = []
        for item in notebook:
            sorted_notes.append(item.values())
        print(tabulate(sorted_notes, headers=self.headers, tablefmt='pipe', stralign='center'))
